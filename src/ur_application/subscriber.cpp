#include "ros/ros.h"
#include "sensor_msgs/JointState.h"

/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void chatterCallback(const sensor_msgs::JointState::ConstPtr& msg)

{
  std::cout<< "function"<<std::endl;

  const std::vector pos = msg -> position;
  const std::vector vel = msg -> velocity;
  const std::vector eff = msg -> effort;
ROS_INFO("postion = [ %f , %f, %f ,%f , %f, %f, %f , %f ,%f ,%f ,%F , %f", pose[0], pose[1], pose[2], pose[3], pose[4], pose[5], pose[6], pose[7], pose[8], pose[9], pose[10], pose[11] );

ROS_INFO("postion = [ %f , %f, %f ,%f , %f, %f, %f , %f ,%f ,%f ,%F , %f", vel[0], vel[1], vel[2], vel[3], vel[4], vel[5], vel[6], vel[7], vel[8], vel[9], vel[10], vel[11] );

ROS_INFO("postion = [ %f , %f, %f ,%f , %f, %f, %f , %f ,%f ,%f ,%F , %f", eff[0], eff[1], eff[2], eff[3], eff[4], eff[5], eff[6], eff[7], eff[8], eff[9], eff[10], eff[11] );
 
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "ur_application");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("joint_states", 1000, chatterCallback);
  ros::spin();

  return 0;
}
