#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "ur_application");

    ros::NodeHandle node;

    auto left_pos_pub = node.advertise<std_msgs::Float64MultiArray>(
        "/left_arm_controller/command", 1);
    auto right_pos_pub = node.advertise<std_msgs::Float64MultiArray>(
        "/right_arm_controller/command", 1);

    std_msgs::Float64MultiArray left_cmd;
    std_msgs::Float64MultiArray right_cmd;
    
    left_cmd.data.resize(6, 0.);
    right_cmd.data.resize(6, 0.);

    const double sample_time = 0.01;
    const double velocity = 0.5;
    ros::Rate rate(1. / sample_time);

    double time = 0.;
    while (ros::ok()) {
        left_cmd.data = {0.8,0.1,0.3,0.8,0.9,0.5};
        right_cmd.data = {0.2,0.1,0.3,0.4,0.5,0.1};
        /*
        for (auto& p : left_cmd.data) {
            p += velocity * sample_time;
        }
        for (auto& p : right_cmd.data) {
            p += velocity * sample_time;
        } 
        */   

        left_pos_pub.publish(left_cmd);
        right_pos_pub.publish(right_cmd);
        
        ros::spinOnce();

        rate.sleep();

        time += sample_time;
        if (time > 2.) {
            break;
        }
    }
}
